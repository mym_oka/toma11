%rebase("base.tpl",title="Bland Choice | TOMA")

<div class="sub_title">
    <img src="static/img/bland_select.png">
</div>
    <article id="wrapper" class="contentwrap">
        <div id="main">
        %for bland in choice_list:
            <section class="lineup">
                <div class="lineup-img">
                    <img src="static/img/{{bland['img']}}">
                </div>
                    <div class="lineup-txt">
                        <h2>{{bland['item_name']}}/{{bland['bland']}}</h2>
                        <div class="likebox">
                            <p class="mental-list">{{bland['mental']}}</p>
                            <form action="/ex_bland" method="POST" class="likeform">
                                <img class="likebun_bla" src="static/img/like_button.png">
                                <input type="hidden" name="id" value="{{bland['id']}}">
                                <span class="liketxt">{{bland['like']}}</span>
                            </form>
                        </div>
                        <p>{{bland['content']}}</p>
                    </div> 
            </section>
        %end

        </div>
        <!-- Sidebar -->
        <div id="sidebar">
            <!-- Mentalで選ぶ -->
            <div class="overbox">
                <div class="material-button alt-2"><span class="shape"></span></div>
                <h3 class="title ">MENTAL<span>で選ぶ</span></h3>
                <form action="/select" method="POST" class="sideform m">
                    <input type="hidden" value="{{bland['bland']}}" name="bland">
                %for mental_name in mental_list:
                    <label class="input">
                        <input type="checkbox" name="mental" value="{{mental_name['mental']}}" class="checkbox01-input">
                        <span class="checkbox01-parts">{{mental_name['mental']}}</span>
                        <span class="spin"></span>
                    </label>
                %end
                    <input type="submit" value="CHOISE">
                </form>
            </div>
        </div>
    </article> 
    <script>
            $(function () {
                $('#wrapper').css({
                    "overflow": "visible"
                })
            });
        </script>
        <script src="/static/js/footerFixed.js"></script>