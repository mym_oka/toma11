%rebase("base.tpl",title="TOMA")

        <article id="wrapper">
            <section id="mental">
                <div class="button-box">
                    <div class="box box1">
                        <h2>MENTAL<br><span>で選ぶ</span></h2>
                    </div> 
                </div>
                <div id="modal_men">
                    <h3>MENTAL<span>で選ぶ</span></h3>
                    <form action="/ex_mental" method="post">
                        %for mental_name in mental_list:
                        <label>
                            <input type="checkbox" name="mental" value="{{mental_name['mental']}}" class="checkbox01-input">
                            <span class="checkbox01-parts">{{mental_name['mental']}}</span>
                        </label>
                        %end
                        
                        <input type="submit" value="CHOICE">
                    </form>
                </div>
            </section>
            <section id="bland">
                <div class="button-box">
                    <div class="box box2">
                        <h2>BLAND<br><span>で選ぶ</span></h2>
                    </div>
                </div>
                <div id="modal_bla">
                    <h3>BLAND<span>で選ぶ</span></h3>
                    <form action="/ex_bland" method="post">
                    %for bland_name in bland_list:
                        <label>
                            <input type="checkbox" name="bland" value="{{bland_name['bland']}}" class="checkbox01-input">
                            <span class="checkbox01-parts">{{bland_name['bland']}}</span>
                        </label>
                    %end

                        <input type="submit" value="CHOICE">
                    </form>
                </div>
            </section>
        </article>