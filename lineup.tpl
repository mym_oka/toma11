%rebase("base.tpl",title="Lineup | TOMA")

    <div class="sub_title">
        <img src="static/img/lineup.png">
    </div>
    <article id="wrapper" class="contentwrap">
        <div id="main">
        
        %for mental in lineup_list:
            <section class="lineup">
                <div class="lineup-img">
                    <img src="static/img/{{mental['img']}}">
                </div>
                    <div class="lineup-txt">
                        <h2>{{mental['item_name']}}/{{mental['bland']}}</h2>
                        <div class="likebox">
                            <p class="mental-list">{{mental['mental']}}</p>
                            <form action="/ex_mental" method="POST" class="likeform">
                                <img class="likebun_men" src="static/img/like_button.png">
                                <input type="hidden" name="id" value="{{mental['id']}}">
                                <span class="liketxt">{{mental['like']}}</span>
                            </form>
                        </div>
                        <p>{{mental["content"]}}</p>
                    </div> 
            </section>
        %end

        </div>
        <div class="material-button alt-2"><span class="shape"></span></div>
        <div id="sidebar">
            <!-- Mentalで選ぶ -->
            <div class="overbox">
                <h3 class="title ">MENTAL<span>で選ぶ</span></h3>
                <form action="/ex_mental" method="POST" class="sideform">
                %for mental_name in mental_list:
                    <label class="input">
                        <input type="checkbox" name="mental" value="{{mental_name['mental']}}" class="checkbox01-input">
                        <span class="checkbox01-parts">{{mental_name['mental']}}</span>
                        <span class="spin"></span>
                    </label>
                %end
                    <input type="submit" value="CHOISE">
                </form>
            </div>
            <!-- Blandで選ぶ -->
            <div class="overbox bottomover">
                <h3 class="title ">BLAND<span>で選ぶ</span></h3>
                <form action="/ex_bland" method="POST" class="sideform">
                %for bland_name in bland_list:
                    <label class="input">
                        <input type="checkbox" name="bland" value="{{bland_name['bland']}}" class="checkbox01-input">
                        <span class="checkbox01-parts">{{bland_name['bland']}}</span>
                        <span class="spin"></span>
                    </label>
                %end
                    <input type="submit" value="CHOISE">
                </form>
            </div>
        </div>
    </article>
    <script>
            $(function () {
                $('#wrapper').css({
                    "overflow": "visible"
                })
            });
        </script>
        <script src="/static/js/footerFixed.js"></script>