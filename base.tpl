<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name=”robots” content=”noindex”>
    <link rel="icon" href="/static/favicon.ico">
    <link rel="stylesheet" href="/static/css/normalize.css">
    <link rel="stylesheet" href="/static/css/style.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    
    <script src="/static/js/common.js"></script>

    <title>{{title or 'No title'}}</title>
</head>
<body>
    <header>
        <div id="head_wrap">
            <div id="logo">
                <a href="/"><img src="/static/img/logo.png" alt=""></a>
            </div>   
            <ul>
                <li><a href="/">Top</a></li>
                <li><a href="/lineup">Lineup</a></li>
                <li><a href="/howto">How to</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </div>
    </header>

{{!base}}

        <footer id="footer">
                <p class="copyright">Copyright &copy; TOA all rights reserved.</p>
        </footer>
</body>
</html>