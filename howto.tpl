%rebase("base.tpl",title="HowTo | TOMA")

<section id="sec3">
    <div class="box-port">
        <div class="midashihaikei">
            <div class="midashiwrap">
                <h1 class="midashi">アロマオイルの使い方</h1>
            </div> 
        </div> 
        <div id="howto_wrap">
            <!-- ここにタイトル画像が入ります -->
            <p class="subtitle">好きなオイルが決まったら、あなたに合ったオイルの使い方で素敵なアロマライフを！</p>
            <div class="portfolio">
                <a href="#"><img src="static/img/scent.jpg" alt="芳香浴"></a>
                <div class="buniti">
                   <h1>芳香浴  </h1>
                    <p>最もポピュラーなのが、アロマ専用の芳香器「アロマディフューザー」やスティックタイプの「リードディフューザー」を使って
                        空間に精油の香りを広げる方法。リビングや寝室で香りを楽しみたい方にオススメ。
                    </p>
                </div>
            </div>
            <div class="portfolio flex_reverse">
                <a href="#"><img src="static/img/howto-thisshu.jpg" alt="ティッシュやコットンにつけて"></a>                
                <div class="buniti">
                   <h1>ティッシュやコットンを使って</h1>
                     <p>最も簡単なアロマオイルの使い方です。  ティッシュやコットン、ハンカチに精油を１～２滴落として、近くにおいて香りを楽しみます。
                         眠る前に枕元に置いたり、バッグやポケットに入れて香りを持ち歩くこともできます♪<br>
                        ！注意！ハンカチを使う場合は、シミになることがあるので、汚れてもよいものを使いましょう。
                        布や木製品に精油が付着するとしみができる場合があるので、机に置いたりバックに入れるときは、気をつけましょう。
                     </p>
                </div>
            </div>
            <div class="portfolio">
                <a href="#"><img src="static/img/howto-stone.jpg" alt="アロマストーン"></a>
                <div class="buniti">
                   <h1>アロマストーン</h1> 
                    <p>素焼きの石や石膏で作られたアロマストーンは精油を垂らすだけで、手軽に使え、インテリアとしても活躍します。
                        電気式のアロマディフューザーに比べると香りの拡散力は弱いですが、ほのかな香りは玄関やトイレ、デスク、車など、狭い空間の芳香にも適しています。
                    </p>
                </div>
            </div>
            <div class="portfolio flex_reverse">
                    <a href="#"><img src="static/img/howto-bath.jpg" alt="アロマバス"></a>
                    <div class="buniti">
                       <h1>アロマバス</h1> 
                        <p>「アロマバス」は、お風呂の湯船に精油を数滴落として香りを楽しみ、肌からも芳香成分を取り入れる方法です。
                            温浴効果もプラスされて、リラクゼーション効果もアップします。
                        </p>
                    </div>
            </div>
        </div>
    </div>            
</section>
    <script>
            $(function () {
                $('#wrapper').css({
                    "overflow": "visible"
                })
            });
        </script>