import os
import sqlite3
from bottle import route, run, debug, template, request, static_file, error, redirect, response , datetime , get
import secrets
secret_cookie = secrets.token_hex()

@route('/')
def index():
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})
    conn.commit()
    conn.close()
    return template("index", mental_list=mental_list, bland_list=bland_list)

@route('/ex_mental', method=["POST"])
def mental_choice():
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        print('ajaxだよ------------------------')
        likeId = request.json
        print(likeId["id"] + likeId["count"])
        if likeId["count"] == "n": 
            conn = sqlite3.connect('toa_test.db')
            c = conn.cursor()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            like = c.fetchone()[0]
            like = like - 1
            c.execute("UPDATE item set like= ? where id=?",(like,likeId["id"]))
            conn.commit()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            re_like = c.fetchone()[0]
            conn.commit()
            conn.close()
            return str(re_like)
        elif likeId["count"] == "null":
            print(type(likeId["id"]))
            conn = sqlite3.connect('toa_test.db')
            c = conn.cursor()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            like = c.fetchone()[0]
            print(like)
            print(type(like))#int確認
            like = like + 1
            print("like:"+ str(like))
            c.execute("UPDATE item set like= ? where id=?",(like,likeId["id"]))
            conn.commit()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            re_like = c.fetchone()[0]
            print(re_like)
            print(type(re_like))
            conn.commit()
            conn.close()
            return str(re_like)
        
    else:
        choice = request.POST.getunicode("mental")
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("SELECT * from item where mental = ? ", (choice,) )
        choice_list=[]
        for row in c.fetchall():
            choice_list.append({"id": row[0], "item_name": row[1], "bland": row[2],"mental":row[3], "content":row[4], "img": row[5], "like": row[6]})
        c.execute("SELECT distinct bland FROM item")
        bland_list=[]
        for row in c.fetchall():
            bland_list.append({"bland": row[0]})
        conn.commit()
        conn.close()
        return template("ex_mental",choice_list=choice_list, bland_list=bland_list)

@route('/ex_bland', method=["POST"])
def bland_choice():
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        print('ajaxだよ------------------------')
        likeId = request.json
        print(likeId["id"] + likeId["count"])
        if likeId["count"] == "n": 
            conn = sqlite3.connect('toa_test.db')
            c = conn.cursor()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            like = c.fetchone()[0]
            like = like - 1
            c.execute("UPDATE item set like= ? where id=?",(like,likeId["id"]))
            conn.commit()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            re_like = c.fetchone()[0]
            conn.commit()
            conn.close()
            return str(re_like)
        elif likeId["count"] == "null":
            print(type(likeId["id"]))
            conn = sqlite3.connect('toa_test.db')
            c = conn.cursor()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            like = c.fetchone()[0]
            print(like)
            print(type(like))#int確認
            like = like + 1
            print("like:"+ str(like))
            c.execute("UPDATE item set like= ? where id=?",(like,likeId["id"]))
            conn.commit()
            c.execute("SELECT like from item where id = ?", (likeId["id"],))
            re_like = c.fetchone()[0]
            print(re_like)
            print(type(re_like))
            conn.commit()
            conn.close()
            return str(re_like)
    else:
        choice = request.POST.getunicode("bland")
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("select * from item where bland = ? ", (choice,) )
        choice_list=[]
        for row in c.fetchall():
            choice_list.append({"id": row[0],"item_name": row[1], "bland": row[2],"mental":row[3], "content":row[4], "img": row[5], "like": row[6]})
        c.execute("SELECT distinct mental FROM item")
        mental_list=[]
        for row in c.fetchall():
            mental_list.append({"mental": row[0]})
        conn.commit()
        conn.close()
        return template("ex_bland",choice_list=choice_list, mental_list=mental_list)

@route('/select', method=["POST"])
def select():
    mental_choice = request.POST.getunicode("mental")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select mental from item where mental = ?", (mental_choice,) )
    mental_choice = c.fetchone()
    conn.close()
    choice = request.POST.getunicode("bland")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select * from item where mental =? and bland = ? ", (mental_choice[0],choice))
    choice_list=[]
    for row in c.fetchall():
        choice_list.append({"id": row[0], "item_name": row[1], "bland": row[2],"mental":row[3], "content":row[4], "img": row[5], "like": row[6]})
    conn.commit()
    conn.close()
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})
    conn.commit()
    conn.close()
    return template("select",choice_list=choice_list,mental_list=mental_list, bland_list=bland_list)

@route('/howto')
def howto():
    return template('howto')

@route('/contact')
def contact():
    return template('contact')
@route("/login", method=["GET", "POST"])
def login():
    if request.method == "GET":
        return template("login")
    else:
        name = request.POST.getunicode("name")
        password = request.POST.getunicode("password")
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("select id from user where name = ? and password = ?", (name, password) )
        user_id = c.fetchone()
        if user_id is not None: 
            user_id = user_id[0]
            response.set_cookie("user_id", user_id, secret='secret_cookie')
            return redirect("add")
        else:
            return template("login")

@route("/logout")
def logout():
    response.set_cookie("user_id", None, secret='secret_cookie')
    return redirect("/login")

@route('/add', method=["GET","POST"])
def add():
    if request.method == "GET":
        name = request.get_cookie("user_id" , secret="secret_cookie")
        if name is None:
            return template("login")    
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("SELECT distinct mental FROM item")
        mental_list=[]
        for row in c.fetchall():
            mental_list.append({"mental": row[0]})
        c.execute("SELECT distinct bland FROM item")
        bland_list=[]
        for row in c.fetchall():
            bland_list.append({"bland": row[0]})
        return template('add' ,bland_list= bland_list, mental_list= mental_list)
    else:
        item_name = request.POST.getunicode("item_name")
        bland = request.POST.getlist("bland")[0]
        mental = request.POST.getlist("mental")[0]
        print(bland)
        print(mental)
        content = request.POST.getunicode("content")
        upload = request.files.get('upload')
        if not upload.filename.lower().endswith(('.png', '.jpg', '.jpeg')):
            return 'png,jpg,jpeg形式のファイルを選択してください'
        save_path = get_save_path()
        upload.save(save_path)
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("insert into item values(null,?,?,?,?,?,0)", (item_name,bland,mental,content,upload.filename))
        conn.commit()
        c.close()
        return redirect('/add')
        
def get_save_path():
    path_dir = "./static/img/"
    return path_dir

@route('/lineup')
def lineup():
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select * from item")
    lineup_list=[]
    for row in c.fetchall():
        lineup_list.append({"id": row[0], "item_name": row[1], "bland": row[2],"mental":row[3], "content":row[4], "img": row[5], "like": row[6]})
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})
    conn.commit()
    c.close()
    return template('lineup', lineup_list = lineup_list, mental_list=mental_list, bland_list=bland_list)

@error(403)
def mistake403(code):
    return template('403')

@error(404)
def mistake404(code):
    return template('404')



@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='static')
# 
# run(host='localhost', port="8089" ,debug=True, reloader=True)
run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
