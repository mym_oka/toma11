$(function() {
    var $win = $(window);

    $win.on('load resize', function() {
        var $hd = $('header').height();
        var $ft = $('footer').height();
        var $r = $ft + $hd;
    var windowWidth = window.innerWidth;
    var windowheight = window.innerHeight;
    var $resolt = windowheight - $r;
    var $resolt_2 = (windowheight - $r) /2;

    if (windowWidth > 768) {
        $('#mental').css('height', $resolt +'px');
        $('#bland').css('height', $resolt +'px');
    } else if (windowWidth < 769) {
        $('#mental').css('height', $resolt_2 + 'px');
        $('#bland').css('height', $resolt_2 + 'px');
    } 
    });

    $('.box1').hover(function() {
        $('#mental').css( { transform: "scale(1.1)" } );
    },function () {
        $('#mental').css( { transform: "scale(1)" } );
    });
    $('.box2').hover(function() {
        $('#bland').css( { transform: "scale(1.1)" } );
    },function () {
        $('#bland').css( { transform: "scale(1)" } );
    });

    // $('.button-box').on('click',function() {
    //     if ($('#modal_bla').is(':visible')) {
    //         $('#modal_bla').fadeOut();
    //     }else if ($('#modal_men').is(':visible')) {
    //         $('#modal_men').fadeOut();
    //     }
    // });
    

    $('.box1').on('click', function() {
        $('#modal_bla').fadeOut();
        $('#modal_bla').css({
            width: '100%',
            height: '100%'
        });
        $('#mental').css( { transform: "scale(1)" } );
        $('#modal_men').fadeIn('slow');
        $('#modal_men').animate({
            width: '85%',
            height: '85%'
        }, 600);
    });
    $('.box2').on('click', function() {
        $('#modal_men').fadeOut();
        $('#modal_men').css({
            width: '100%',
            height: '100%'
        });
        $('#bland').css( { transform: "scale(1)" } );
        $('#modal_bla').fadeIn('slow');
        $('#modal_bla').animate({
            width: '85%',
            height: '85%'
        }, 600);
    });

    //-------------------
        $('.material-button').on('click', function() {
            if (!$(".overbox").hasClass('active')) {
                $(".overbox").removeClass('hidden')
                $(".overbox").addClass('active');
                $(this).removeAttr("style");
                $(this).css({
                    "transform": "rotate(45deg)",
                    "background": "transparent",
                    // "top": "160px",
                    "z-index": "100"
                });
                $(this).animate({
                    "top": "160px"
                },700);
            
                $('.title').fadeIn(300);
                $('.sideform').fadeIn(300);
                // setTimeout(function() {
                //     $(".overbox").css({
                //        "overflow": "initial"
                //     })
                //  }, 600)
    
            } else if (!$(".overbox").hasClass('hidden')) {
                $(".overbox").removeClass('active')
                $(".overbox").addClass('hidden');
                $(this).removeAttr("style");
                $(this).css({
                    "transform": "rotate(0deg)",
                    "background": "rgba(0,0,0,0.4)"
                    // "bottom": "50px"
                })
                $(this).animate({
                    "bottom": "50px"
                },700);
                $('.title').fadeOut(300);
                $('.sideform').fadeOut(300);
                // setTimeout(function() {
                //     $(".overbox").css({
                //        "overflow": "initial"
                //     })
                //  }, 500)
    
            }
           
        });

        $("#modal_men input[type=checkbox]").click(function(){
            var $count = $("#modal_men input[type=checkbox]:checked").length;
            var $not = $('#modal_men input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });
        $("#modal_bla input[type=checkbox]").click(function(){
            var $count = $("#modal_bla input[type=checkbox]:checked").length;
            var $not = $('#modal_bla input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });
        $(".form_men input[type=checkbox]").click(function(){
            var $count = $(".form_men input[type=checkbox]:checked").length;
            var $not = $('.form_men input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });
        $(".form_bla input[type=checkbox]").click(function(){
            var $count = $(".form_bla input[type=checkbox]:checked").length;
            var $not = $('.form_bla input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });
        $(".m input[type=checkbox]").click(function(){
            var $count = $(".m input[type=checkbox]:checked").length;
            var $not = $('.m input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });
        $(".b input[type=checkbox]").click(function(){
            var $count = $(".b input[type=checkbox]:checked").length;
            var $not = $('.b input[type=checkbox]').not(':checked')
            if($count >= 1) {
                $not.attr("disabled",true);
            }else{
                $not.attr("disabled",false);
            }
        });


// -----------------ajax---------------------------------
        var num = 0;
        $('.likebun_men').on('click', function() {
            var elm = $(this);
            elm.data("click", ++num);
            var click = elm.data('click');
            if (click % 2 == 0) {
                $.ajax ({
                    url: '/ex_mental',
                    type: 'POST',
                    contentType:'application/json',
                    data:JSON.stringify({
                        "id": $(this).next($('input:hidden[name="id"]')).val(),
                        "count": "n"
                    })
                    // data: $(this).next($('input:hidden[name="id"]'))
                    }).done(function(data) {
                        console.log(data);
                        console.log($(this));
                        // if (data == 0) {
                        //     elm.siblings('.liketxt').text('1');
                        // } else {
                        elm.siblings('.liketxt').text(data);
                        // }
                    }).fail(function() {
                        console.log("miss");
                    });
                } else {
                    $.ajax ({
                        url: '/ex_mental',
                        type: 'POST',
                        contentType:'application/json',
                        data:JSON.stringify({
                            "id": $(this).next($('input:hidden[name="id"]')).val(),
                            "count": "null"
                        })
                        // data: $(this).next($('input:hidden[name="id"]'))
                        }).done(function(data) {
                            console.log(data);
                            console.log($(this));
                            // if (data == 0) {
                            //     elm.siblings('.liketxt').text('1');
                            // } else {
                            elm.siblings('.liketxt').text(data);
                            // }
                        }).fail(function() {
                            console.log("miss");
                        });
                }

        });
// --------------   /ex_bland ajax ----------------------------
        $('.likebun_bla').on('click', function() {
            var elm = $(this);
            elm.data("click", ++num);
            var click = elm.data('click');
            if (click % 2 == 0) {
                console.log($("偶数だよーーーーーーーーーーーーーーーー"));
                $.ajax ({
                    url: '/ex_bland',
                    type: 'POST',
                    contentType:'application/json',
                    data:JSON.stringify({
                        "id": $(this).next($('input:hidden[name="id"]')).val(),
                        "count": "n"
                    })
                    }).done(function(data) {
                        console.log(data);
                        console.log($(this));
                        // if (data == 0) {
                        //     elm.siblings('.liketxt').text('1');
                        // } else {
                        elm.siblings('.liketxt').text(data);
                        // }
                    }).fail(function() {
                        console.log("miss");
                    });
                } else {
                    console.log($("奇数だよーーーーーーーーーーーーーーーー"));
                    $.ajax ({
                        url: '/ex_bland',
                        type: 'POST',
                        contentType:'application/json',
                        data:JSON.stringify({
                            "id": $(this).next($('input:hidden[name="id"]')).val(),
                            "count": "null"
                        })
                        // data: $(this).next($('input:hidden[name="id"]'))
                        }).done(function(data) {
                            console.log(data);
                            console.log($(this));
                            // if (data == 0) {
                            //     elm.siblings('.liketxt').text('1');
                            // } else {
                            elm.siblings('.liketxt').text(data);
                            // }
                        }).fail(function() {
                            console.log("miss");
                        });
                }

        });








        

        


});