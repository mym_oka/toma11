%rebase("base.tpl",title="Item追加ページ | TOMA")

    <div class="sub_title">
        <img src="static/img/lineup_add.png">
    </div>
    <article id="wrapper" class="contentwrap">
        <div class="">
            <form action="/add" method="POST" enctype="multipart/form-data">
                <dl class="top30">
                    <dt>ブランド名：</dt>
                    <dd class="custom">
                        %for bland_name in bland_list:
                        <label>
                            <input type="checkbox" name="bland" value="{{bland_name['bland']}}">
                            <span>{{bland_name['bland']}}</span>
                        </label>  
                        %end
                    </dd>
                </dl>
                <dl class="top30">
                    <dt>商品名：</dt>
                    <dd class="custom"><input type="text" name="item_name" size="30" placeholder="商品名"></dd>
                </dl>
                <dl class="top30">
                    <dt>メンタル：</dt>
                    <dd class="txtarea">
                        %for mental_name in mental_list:
                        <label>
                            <input type="checkbox" name="mental" value="{{mental_name['mental']}}">
                            <span>{{mental_name['mental']}}</span>
                        </label>
                        %end
                    </dd>
                </dl>
                <dl class="top30">
                    <dt>説明文：</dt>
                    <dd class="txtarea">
                        <textarea name="content" rows="6" cols="40"></textarea>
                    </dd>
                </dl>
                <dl class="custom top30">
                    <dt>商品画像：</dt>
                    <dd>
                        <input type="file" name="upload">
                    </dd>
                </dl>
                <dl class="custom">
                    <dd><input type="submit" value="登録" class="con_submit"></dd>
                </dl>
            </form>
        </div>
    </article>
    <div id="logout">
        <a href="/logout">ログアウト</a>
    </div>
    <script>
            $(function () {
                $('#wrapper').css({
                    "overflow": "visible"
                })
            });
        </script>
        <script src="/static/js/footerFixed.js"></script>