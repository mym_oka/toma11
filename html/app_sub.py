import os
import sqlite3
# 課題2の答えはここ(一番右) 時間を取得するためにimportする
from bottle import route, run, debug, template, request, static_file, error, redirect, response , datetime , get
# secrets.token_hex([nbytes=None])
# 十六進数のランダムなテキスト文字列を返します。文字列は nbytes のランダムなバイトを持ち、各バイトは二つの十六進数に変換されます。nbytes が None の場合や与えられなかった場合は妥当なデフォルト値が使われます。
import secrets
secret_cookie = secrets.token_hex()

@route('/')
def index():
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    # Mental
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    # Bland
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})

    conn.commit()
    conn.close()

    return template("index", mental_list=mental_list, bland_list=bland_list)


# Mentalで選ぶ→検索結果画面
@route('/ex_mental', method=["POST"])
def mental_choice():
    choice = request.POST.getunicode("mental")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select item_name,bland,mental,content,img from item where mental = ? ", (choice,) )
    # Mentalで選んだモノをリスト表示
    choice_list=[]
    for row in c.fetchall():
        choice_list.append({"item_name": row[0], "bland": row[1],"mental":row[2], "content":row[3], "img": row[4]})

# Mentalで抽出リスト＋サイドバー（ブランド一覧）
    c.execute("SELECT distinct bland FROM item")
    # fetchoneはタプル型
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})
    conn.commit()
    conn.close()
    return template("ex_mental",choice_list=choice_list, bland_list=bland_list)

# Blandで選ぶ→検索結果画面
@route('/ex_bland', method=["POST"])
def bland_choice():
    choice = request.POST.getunicode("bland")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select item_name,bland,mental,content,img from item where bland = ? ", (choice,) )
    # blandで選んだモノをリスト表示
    choice_list=[]
    for row in c.fetchall():
        choice_list.append({"item_name": row[0], "bland": row[1],"mental":row[2], "content":row[3], "img": row[4]})

# Mentalで抽出リスト＋サイドバー（メンタル一覧）
    c.execute("SELECT distinct mental FROM item")
    # fetchoneはタプル型
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    conn.commit()
    conn.close()
    return template("ex_bland",choice_list=choice_list, mental_list=mental_list)


@route('/select', method=["POST"])
def select():
    mental_choice = request.POST.getunicode("mental")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select mental from item where mental = ?", (mental_choice,) )
    mental_choice = c.fetchone()

    conn.close()

    choice = request.POST.getunicode("bland")
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select item_name,bland,mental,content,img from item where mental =? and bland = ? ", (mental_choice[0],choice ))

    # Mentalで選んだモノをリスト表示
    choice_list=[]

    for row in c.fetchall():
        choice_list.append({"item_name": row[0], "bland": row[1],"mental":row[2], "content":row[3], "img": row[4]})

    conn.commit()
    conn.close()

    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    # Mental
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    # Bland
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})

    conn.commit()
    conn.close()

    return template("select",choice_list=choice_list,mental_list=mental_list, bland_list=bland_list)


@route('/howto')
def howto():
    return template('howto')


@route('/contact')
def contact():
    return template('contact')

# GET  /login => ログイン画面を表示
# POST /login => ログイン処理をする
@route("/login", method=["GET", "POST"])
def login():
    if request.method == "GET":
        return template("login")
    else:
    # ブラウザから送られてきたデータを受け取る
        name = request.POST.getunicode("name")
        password = request.POST.getunicode("password")

    # ブラウザから送られてきた name ,password を userテーブルに一致するレコードが
    # 存在するかを判定する。レコードが存在するとuser_idに整数が代入、存在しなければ nullが入る
        conn = sqlite3.connect('toa_test.db')
        c = conn.cursor()
        c.execute("select id from user where name = ? and password = ?", (name, password) )
        user_id = c.fetchone()
    if user_id is not None: 
        user_id = user_id[0]
    # Mental
        c.execute("SELECT distinct mental FROM item")
        mental_list=[]
        for row in c.fetchall():
            mental_list.append({"mental": row[0]})

    # Bland
        c.execute("SELECT distinct bland FROM item")
        bland_list=[]
        for row in c.fetchall():
            bland_list.append({"bland": row[0]})
        conn.commit()
        conn.close()
        
        response.set_cookie("user_id", user_id, secret='secret_cookie')
        return template("add", mental_list=mental_list, bland_list = bland_list)
    else:
        # ログイン失敗すると、ログイン画面に戻す
        return template("login")

@route("/logout")
def logout():
    # ログアウトはクッキーに None を設定してあげるだけ
    response.set_cookie("user_id", None, secret='secret_cookie')
    return redirect("/login") # ログアウト後はログインページにリダイレクトさせる


@route('/lineup')
def lineup():
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    c.execute("select * from item order by id DESC")
    lineup_list=[]
    for row in c.fetchall():
        lineup_list.append({"id": row[0], "item_name": row[1], "bland": row[2], "mental":row[3], "content":row[4], "img":row[5],})
    # 以下、Sidebar
    # Mental
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})
    # Bland
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})

    conn.commit()
    c.close()
    return template('lineup', lineup_list = lineup_list, mental_list=mental_list, bland_list=bland_list)


@route('/add', method=["POST"])
def add():
    conn = sqlite3.connect('toa_test.db')
    c = conn.cursor()
    # Mental
    c.execute("SELECT distinct mental FROM item")
    mental_list=[]
    for row in c.fetchall():
        mental_list.append({"mental": row[0]})

    # Bland
    c.execute("SELECT distinct bland FROM item")
    bland_list=[]
    for row in c.fetchall():
        bland_list.append({"bland": row[0]})

        
    print(bland_list)
    conn.commit()
    c.close()

    # item_name = request.POST.getunicode("item_name")
    # conn = sqlite3.connect('toa_test.db')
    # c = conn.cursor()

    # c.execute("insert into item values(null,?)", (item_name,))

    return template('add' ,bland_list= bland_list, mental_list= mental_list)

@error(403)
def mistake403(code):
    return template('403')

@error(404)
def mistake404(code):
    return template('404')



@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='static')

run(host='localhost', port="8089" ,debug=True, reloader=True)
