%rebase("base.tpl",title="Contact | TOMA")

    <div class="sub_title">
        <img src="static/img/contact.png">
    </div>
    <article id="wrapper" class="contentwrap">
        <div class="con_form">
            <form action="" method="post">
                <dl>
                    <dt>氏名：</dt>
                    <dd class="custom"><input type="text" name="name" size="30" placeholder="Name"></dd>
                </dl>
                <dl>
                    <dt>E-mail：</dt>
                    <dd class="custom"><input type="text" name="email" size="30" type="email" placeholder="abc@email.com"></dd>
                </dl>
                <dl>
                    <dt>内容：</dt>
                    <dd class="custom"><textarea name="text" rows="6" cols="40"></textarea></dd>
                </dl>
                <dl>
                    <dt></dt>
                    <dd><input type="submit" value="送信" class="con_submit"></dd>
                </dl>
            </form>
        </div>
    </article>  
    <script>
        $(function () {
            $('#wrapper').css({
                "overflow": "visible"
            })
        });
    </script>
    <script src="/static/js/footerFixed.js"></script>